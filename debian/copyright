Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/       
Upstream-Name: Criterion
Upstream-Contact: Franklin "Snaipe" Mathieu <http://snai.pe/>
Source: https://github.com/Snaipe/Criterion
Disclaimer: <text> This file is offered as-is, without any warranty. </text>
Copyright: Copyright © 2015-2017 Franklin "Snaipe" Mathieu <http://snai.pe/>
License: MIT
Comment:  created on 2020-08-25T16:09:57Z with FOSSology


Files: *
Copyright: Copyright © 2015-2017 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2015-2016 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2016-2018 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2017 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2016 Matthias "ailu" Günzel <a1lu@arcor.de>
           Copyright (c) 2016 Matthias "ailu" Günzel <a1lu@arcor.de>
           Copyright (c) 2007 Russ Cox.
           Copyright © 2020 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright (c) 2014 George Spelvim <linux@horizon.com>
           Copyright © 2015-2018 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2016 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2018 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright © 2017-2018 Franklin "Snaipe" Mathieu <http://snai.pe/>
           Copyright (C) 2015 Franklin "Snaipe" Mathieu <franklinmathieu@gmail.com>
           Copyright (C) 2016 <a1lu@arcor.de>
           copyright 1995,2000 Simon Tatham.
License: MIT

Files: dev/*
Copyright: Franklin "Snaipe" Mathieu <franklinmathieu@gmail.com>
           László "MrAnno" Várady <laszlo.varady@balabit.com>
License: WTFPL

Files: dependencies/valgrind/include/valgrind/valgrind.h
Copyright: Copyright (C) 2000-2013 Julian Seward. All rights reserved.
License: bzip2

Files: dependencies/debugbreak/*
Copyright: Copyright (c) 2011-2018, Scott Tsai
           Copyright (c) 2011-2016, Scott Tsai
License: BSD-2-Clause

Files: dependencies/klib/*
Copyright: Copyright (c) 2008, 2009, 2011 by Attractive Chaos <attractor@live.co.uk>
License: MIT

License: MIT
 The MIT License (MIT)
 .
 Copyright © 2015-2017 Franklin "Snaipe" Mathieu <http://snai.pe/>
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: WTFPL
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004
 .
 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 .
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
  0. You just DO WHAT THE FUCK YOU WANT TO.

License: bsd-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1.  Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 .
 2.  Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: bzip2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1.  Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 2.  The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software.  If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
 3.  Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
 4.  The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
